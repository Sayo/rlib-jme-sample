package com.ss.client.stage.impl;

import com.jme3.app.state.AbstractAppState;
import com.ss.client.stage.Stage;

/**
 * The base implementation of a stage of the game.
 *
 * @author JavaSaBr
 */
public abstract class AbstractStage extends AbstractAppState implements Stage {
}
