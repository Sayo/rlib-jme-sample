package com.ss.client.ui.css;

/**
 * The interface with available css classes.
 * 
 * @author JavaSaBr
 */
public interface CSSClasses {

    String LOGIN_AUTH_PANEL_FIELD = "loginAuthPanelField";
    String LOGIN_AUTH_PANEL_LABEL = "loginAuthPanelLabel";
    String LABEL_BUTTON = "labelButton";

    String BUTTON_SIZE_1 = "button-size-1";
}