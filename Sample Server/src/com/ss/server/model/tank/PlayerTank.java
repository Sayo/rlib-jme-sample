package com.ss.server.model.tank;

import com.ss.server.model.impl.AbstractGameObject;

/**
 * Реализация танка.
 */
public class PlayerTank extends AbstractGameObject {

    public PlayerTank(int objectId) {
        super(objectId);
    }
}
